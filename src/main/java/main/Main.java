package main;

import beans.User;
import dbService.DBServiceImpl;
import hibernate.HDBService;
import hibernate.HDBServiceImpl;
import org.openjdk.jmh.annotations.Benchmark;


public class Main {

    public static void main(String[] args) {
        testJDBC();
        testJDBC();
        testHibernate();
        testHibernate();
    }

    @Benchmark
    public static void testJDBC(){
        Long startTime = System.currentTimeMillis();
        final DBServiceImpl dbService = new DBServiceImpl();
        final User user = dbService.selectUser(1);
        Long endtime = System.currentTimeMillis();
        System.out.printf("Время выполнения запроса на чистом jdbc: %d\n", (endtime - startTime));
    }

    @Benchmark
    public static void testHibernate(){
        Long hiberStartTime = System.currentTimeMillis();
        final HDBService hdbService = new HDBServiceImpl();
        final User huser = hdbService.read(1);
        Long hiberEndtime = System.currentTimeMillis();
        System.out.printf("Время выполнения запроса c hibernate: %d\n", (hiberEndtime - hiberStartTime));
    }

}

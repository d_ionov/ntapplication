package beans;


public class Error {

    private String date;

    private String discription;

    public Error(String time, String discription) {
        this.date = time;
        this.discription = discription;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    @Override
    public String toString() {
        return "Error{" +
                "date=" + date +
                ", discription='" + discription + '\'' +
                '}';
    }
}

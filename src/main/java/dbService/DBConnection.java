package dbService;


import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static Connection connection;

    private static String driverName = "com.mysql.jdbc.Driver";

    private DBConnection(){}

    public static Connection instance(){
        if(connection == null) {
            connection = createConnection();
        }
        return connection;
    }

    private static Connection createConnection(){
        Connection connection = null;
        try {
            Driver driver = (Driver) Class.forName(driverName).newInstance();
            DriverManager.registerDriver(driver);
            StringBuilder url = new StringBuilder(128);
                 url.append("jdbc:mysql://")
                    .append("localhost:")
                    .append("3306/")
                    .append("ntapplication_schema?")
                    .append("user=root&")
                    .append("password=*****");
            connection = DriverManager.getConnection(url.toString());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void setDriverName(String driverName) {}

    public static String getDriverName(){
        return driverName;
    }


}

package dbService.executor;

//SELECT * FROM ntapplication_schema.errors WHERE date = "2017-09-09 00:00:01"
import dbService.DBConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Executor {

    public static int execUpdate(final String update){
        int result = 0;
        try(final Statement statement = DBConnection.instance().prepareStatement(update)) {
            result = statement.getUpdateCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <T> T execQuery(final String query, final ResultHandler handler){
        T result = null;
        try(final PreparedStatement statement = DBConnection.instance().prepareStatement(query)) {
            statement.execute();
            final ResultSet resultSet = statement.getResultSet();
            result = handler.handle(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


}

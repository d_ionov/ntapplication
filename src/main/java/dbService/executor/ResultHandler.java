package dbService.executor;


import java.sql.ResultSet;

public interface ResultHandler {

    <T> T handle(ResultSet resultSet);

}

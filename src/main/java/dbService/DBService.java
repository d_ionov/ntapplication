package dbService;


public interface DBService{

    int update(final String updateObject);

    <T> T select(final String selectObject);

    int delete(final String deleteObject);

}

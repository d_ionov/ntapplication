package dbService;

import beans.Error;
import beans.User;
import dbService.dao.ErrorsDao;
import dbService.dao.UsersDao;

public class DBServiceImpl implements DBService{

    @Override
    public int update(final String updateObject) {
        return 0;
    }

    @Override
    public Error select(final String selectObject) {
        return selectError(selectObject);
    }

    @Override
    public int delete(final String deleteObject) {
        return 0;
    }

    public Error selectError(final String dateError){
        return ErrorsDao.getError(dateError);
    }

    public User selectUser(final long id){
        return UsersDao.getUser(id);
    }

}

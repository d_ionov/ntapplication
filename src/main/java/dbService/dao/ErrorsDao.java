package dbService.dao;

import dbService.executor.Executor;
import dbService.executor.ResultHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import beans.Error;

public class ErrorsDao {

    public static int insertError(final String dateError, final String descriptionError){
        final String insert = "INSERT INTO ntapplication_schema.errors (date, discription) " +
                              "VALUES('" + dateError + "', " + descriptionError + ")";
        return Executor.execUpdate(insert);
    }

    public static Error getError(final String dateError){
        final String query = "SELECT * FROM ntapplication_schema.errors WHERE date = \"" + dateError + "\"";
        return Executor.execQuery(query, new ResultHandler() {
            @Override
            public <T> T handle(ResultSet resultSet) {
                T result = null;
                try {
                    resultSet.next();
                    result = (T) new Error(resultSet.getString(1), resultSet.getString(2));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return result;
            }
        });
    }

}

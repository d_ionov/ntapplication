package dbService.dao;

import beans.Error;
import beans.User;
import dbService.executor.Executor;
import dbService.executor.ResultHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersDao{

    public static User getUser(final long id){
        final String query = "SELECT * FROM ntapplication_schema.users WHERE id = \"" + id + "\"";
        return Executor.execQuery(query, new ResultHandler() {
            @Override
            public <T> T handle(ResultSet resultSet) {
                T result = null;
                try {
                    resultSet.next();
                    result = (T) new User(resultSet.getInt(1), resultSet.getString(2));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return result;
            }
        });
    }

}

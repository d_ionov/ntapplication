package hibernate;


import beans.User;

public interface HDBService {

    User read(long id);

    User readByName(String name);

}
